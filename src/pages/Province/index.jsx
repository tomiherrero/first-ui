import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Edit from './Edit';
import List from './List';


const Province = ({match: {path}}) => (
    <Switch>
        <Route path = {`${path}/new`} strict component = {Edit}/>
        <Route path = {`${path}/:id`} strict component = {Edit}/>
        <Route path = {`${path}`} component = {List}/>
    </Switch>
);

export default Province; 