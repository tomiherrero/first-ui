import React, {Component} from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import {Link} from 'react-router-dom'; 
import {connect} from 'react-redux';
import {fetchProvinceRequested, deleteProvinceRequested} from '../../actions/province';
import {Container, Row, Col, Button} from 'reactstrap';


class Province extends Component {
    componentDidMount(){
        this.props.requestProvince();
    }
    render () {
        const {headers, documents} = this.props;
        return(
            <Container>
                <Row>
                    <Col sm="8">
                        <Title title = "Provincias"/>
                        <hr/>
                    </Col>
                    <Col sm="4" className="float-right mt-2">
                        <Button outline tag= {Link} color= "primary" to = "./province/new">Agregar Provincia</Button>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table 
                            {...{documents, headers, linkTo: '/province'}}
                            onDelete= {id => this.props.deleteProvince(id)}    
                        />
                    </Col>
                </Row>
            </Container>
        )
    }
}
const mapStateToProps = state => ({ 
    headers: state.province.headers,
    documents: state.province.provinces
})

const mapDispatchToProps = dispatch => ({
    requestProvince: () => dispatch(fetchProvinceRequested()),
    deleteProvince: id => dispatch(deleteProvinceRequested(id))
})

export default connect(mapStateToProps,  mapDispatchToProps)(Province);