import React, {Component} from 'react'; 
import {InputText} from '../../components/Form';
import {connect} from 'react-redux';
import {updateProvince, submitProvinceRequested, fetchProvincesRequested} from '../../actions/province'
import {Form, Button, Container, Row, Col} from 'reactstrap';

class Edit extends Component {

    componentDidMount(){
        const {id} = this.props.match.params;
        if(id) {
            this.props.fetchProvinces(id);
        }
    }
        handleChange(obj){
        const {province} = this.props;
        Object.assign(province, obj);
        this.props.updateProvinces(province);
        this.forceUpdate();
    }
    render() {
        const {
            province: {        
                name,
                code
            }
        } = this.props;
        return (
            <Container>
                <Row>
                    <Col sm= {{offset:2, size: 5}}>
                        <Form>
                            <h1>Edición de Provincias</h1>
                            <hr/>
                            <InputText 
                                    key = "name"
                                    label="Nombre :"
                                    value = {name}
                                    onChange=  {({target: {value}}) => this.handleChange({name: value})}

                                />
                            <InputText 
                                    key = "code"
                                    label = "Código :"
                                    value= {code}
                                    onChange=  {({target: {value}}) => this.handleChange({code: value})}

                                />
                                <Button outline color = 'success'
                                onClick = {()=> this.props.submit()}>
                                    Guardar
                                </Button>
                            <br/>
                            <br/>
                        </Form>
                    </Col>
                </Row>
            </Container>
            

        )
    }
}
const mapStateToProps = state => ({
    province: state.province.currentProvince,
});
const mapDispatchToProps = dispatch => ({
    fetchProvinces: id => dispatch(fetchProvincesRequested(id)),
    updateProvinces: provinces => dispatch (updateProvince(provinces)),
    submit: () => dispatch(submitProvinceRequested())


});
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Edit);