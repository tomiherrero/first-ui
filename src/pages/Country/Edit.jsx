import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import {InputText} from '../../components/Form';
import {updateCountry, submitCountryRequested, fetchCountriesRequested} from '../../actions/country'
import {Form, Button, Container, Row, Col} from 'reactstrap';

class Edit extends Component {
    
    componentDidMount() {
        const {id} = this.props.match.params; 
        if (id) {
            this.props.fetchCountryOne(id);
        }
    }

    handleChange(obj) {
        const {country} = this.props;
        Object.assign(country, obj);
        this.props.updateCountries(country);
        this.forceUpdate();

    }
    render(){
        const {
            country: {
                name, 
                code
            }
        } = this.props; 
        return(
            <Container>
                <Row>
                    <Col sm= {{offset:2 , size:5}}>
                        <Form>
                            <h1>Edición de Países</h1>
                            <hr/>
                            <InputText 
                                    key = 'name'
                                    label= 'Nombre: '
                                    value = {name}
                                    onChange=  {({target: {value}}) => this.handleChange({name: value})}

                            />
                            <InputText 
                                    key = 'code'
                                    label = 'Código: '
                                    value = {code}
                                    onChange=  {({target: {value}}) => this.handleChange({code: value})}

                            />
                            <Button outline color='success'
                                onClick ={() => this.props.submit()}>
                                    Guardar 
                            </Button>
                        </Form>                       
                    </Col>
                </Row>
            </Container>
        )
    }
};


const mapStateToProps = state => ({
    country: state.country.currentCountry
})

const mapDispatchToProps = dispatch => ({
    fetchCountryOne: id => dispatch(fetchCountriesRequested(id)),
    updateCountries: countries =>  dispatch(updateCountry(countries)),
    submit: () => dispatch(submitCountryRequested())
})


export default connect(mapStateToProps, mapDispatchToProps)(Edit)