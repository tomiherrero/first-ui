import React, {Component} from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import {connect} from 'react-redux';
import {fetchCountryRequested,  deleteCountryRequested} from '../../actions/country';
import {Link} from 'react-router-dom';
import {Container, Row, Col, Button } from 'reactstrap';

class Country extends Component {

  async componentDidMount() {
    this.props.requestCountry();
  }
  
  render () { 
    const {documents, headers} = this.props;
    return (
    
      <Container>
          <Row>
            <Col sm="8">    
                <Title title= "Paises"/>
                <hr/>
            </Col>
            <Col sm="4" className="float-right mt-2">
              <Button outline tag={Link} color= "primary" to = '/country/new'>Agregar País</Button>
            </Col>
          </Row>
          <Row>
            <Col>
                <Table {... {documents, headers, linkTo: '/country'}} 
                onDelete ={id => this.props.deletedCountry(id)}
                />
            </Col>
          </Row>
      </Container>
    );
  }
}

const mapStateToProps = state =>({
  headers: state.country.headers,
  documents: state.country.countries
}) 

const mapDispatchToProps = dispatch => ({
  requestCountry: () => dispatch(fetchCountryRequested()),
  deletedCountry: id => dispatch(deleteCountryRequested(id))
})
export default connect(mapStateToProps, mapDispatchToProps)(Country);
