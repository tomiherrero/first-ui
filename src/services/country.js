import HTTP from './http';

export default class Country{ 

    static async apiSave(country) {

            if(country.id) {
               return HTTP.put(`api/country/${country.id}`, country)
            } else {
                return HTTP.post('api/country', country)
            }
          }
      
     static async apiCallOne (id) {
          return HTTP.get(`api/country/${id}`)
     }
     static async apiCall() {
        return HTTP.get('api/country')
     }
      
     static async apiDelete(id){
        return HTTP.delete(`api/country/${id}`)
      }

}