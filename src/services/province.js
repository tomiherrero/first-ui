
import HTTP from './http';
export default class Provinces{ 

    static async apiSave(province) {

            if(province.id) {
                return HTTP.put(`api/province/${province.id}`, province)
            } else {
                return HTTP.post('api/province', province)
            }
        }
     static async apiCallOne (id) {
          return HTTP.get(`api/province/${id}`)
     }
     static async apiCall() {
        return HTTP.get('api/province') 
      }
      
     static async apiDelete(id){
       return HTTP.delete(`api/province/${id}`)
     }
}