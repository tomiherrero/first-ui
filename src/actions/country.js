export const FETCH_COUNTRY_REQUESTED = 'FETCH_COUNTRY_REQUESTED'; // >> accion Acciones 
export const FETCH_COUNTRY_SUCCEEDED = 'FETCH_COUNTRY_SUCCEEDED'; // << reaccion Acciones

export const fetchCountryRequested = () => ({type: FETCH_COUNTRY_REQUESTED}); // >> disparadores
export const fetchCountrySucceeded = countries => ({type: FETCH_COUNTRY_SUCCEEDED, countries}); //<< disparadores 

export const FETCH_COUNTRIES_REQUESTED = 'FETCH_COUNTRIES_REQUESTED'; // >> accion Acciones 
export const FETCH_COUNTRIES_SUCCEEDED = 'FETCH_COUNTRIES_SUCCEEDED'; // << reaccion Acciones

export const fetchCountriesRequested = id => ({type: FETCH_COUNTRIES_REQUESTED, id}); // >> disparadores
export const fetchCountriesSucceeded = country => ({type: FETCH_COUNTRIES_SUCCEEDED, country}); //<< disparadores 

export const UPDATE_COUNTRY = 'UPDATE_COUNTRY';
export const updateCountry  = countries => ({type: UPDATE_COUNTRY, countries}); 

export const SUBMIT_COUNTRY_REQUESTED = 'SUBMIT_COUNTRY_REQUESTED'; 
export const submitCountryRequested = () => ({type: SUBMIT_COUNTRY_REQUESTED});


export const SUBMIT_COUNTRY_SUCCEEDED = 'SUBMIT_COUNTRY_SUCCEEDED';
export const submitCountrySucceeded= (status, data) => ({type: SUBMIT_COUNTRY_SUCCEEDED, status, data});

export const DELETE_COUNTRY_REQUESTED =  'DELETE_COUNTRY_REQUESTED';
export const DELETE_COUNTRY_SUCCEEDED = 'DELETE_COUNTRY_SUCCEEDED';

export const deleteCountryRequested = id => ({type: DELETE_COUNTRY_REQUESTED, id });
export const deleteCountrySucceeded = () => ({type: DELETE_COUNTRY_SUCCEEDED});