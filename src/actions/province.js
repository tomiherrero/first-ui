
export const FETCH_PROVINCE_REQUESTED = 'FETCH_PROVINCE_REQUESTED';
export const FETCH_PROVINCE_SUCCEEDED = 'FETCH_PROVINCE_SUCCEEDED'; 

export const fetchProvinceRequested = () =>({type: FETCH_PROVINCE_REQUESTED});
export const fetchProvinceSucceeded = provinces => ({type: FETCH_PROVINCE_SUCCEEDED, provinces});

export const FETCH_PROVINCES_REQUESTED = 'FETCH_PROVINCES_REQUESTED';
export const FETCH_PROVINCES_SUCCEEDED = 'FETCH_PROVINCES_SUCCEEDED'; 

export const fetchProvincesRequested = id => ({type: FETCH_PROVINCES_REQUESTED, id});
export const fetchProvincesSucceeded = province => ({type: FETCH_PROVINCES_SUCCEEDED, province});


export const UPDATE_PROVINCE = 'UPDATE_PROVINCE';
export const updateProvince = provinces =>({type: UPDATE_PROVINCE, provinces});

export const SUBMIT_PROVINCE_REQUESTED = 'SUBMIT_PROVINCE_REQUESTED';
export const submitProvinceRequested = () => ({type: SUBMIT_PROVINCE_REQUESTED});

export const SUBMIT_PROVINCE_SUCCEEDED = 'SUBMIT_PROVINCE_SUCCEEDED';
export const submitProvinceSucceeded= (status, data) => ({type: SUBMIT_PROVINCE_SUCCEEDED, status, data});


export const DELETE_PROVINCE_REQUESTED = 'DELETE_PROVINCE_REQUESTED';
export const DELETE_PROVINCE_SUCCEEDED = 'DELETE_PROVINCE_SUCCEEDED';

export const deleteProvinceRequested = id => ({type: DELETE_PROVINCE_REQUESTED, id});
export const deleteProvinceSucceeded = ()  => ({type: DELETE_PROVINCE_SUCCEEDED});