import React from 'react';
import {Link} from 'react-router-dom';
import {Table, Button, ButtonGroup} from 'reactstrap';

const TableComponent = ({documents, headers, linkTo, onDelete, primaryKey = 'id'}) => (
    <Table color= "light "size="sm" responsive>
        <thead>
            <tr>
                {headers && headers.map(header => (<th>{header.label}</th>))}
                {linkTo && (<th>  </th>)}
            </tr>
        </thead>
        <tbody>
                {documents && headers && documents.map(documents => ( 
            <tr> 
                    {headers.map(header => (<td>{documents[header.key]} </td>
                    ))}
                <td> 
                    <ButtonGroup>
                        {linkTo && ( <Button outline size= "sm" color ="primary" tag ={Link} to = {`${linkTo}/${documents[primaryKey]}`}> Editar </Button>)}
                        {onDelete && (<Button outline size="sm" color ="danger" onClick={() => onDelete(documents[primaryKey])}>Borrar</Button>)}
                    </ButtonGroup>
                    
                </td>
                  
            </tr>
            ))}
        </tbody>
    </Table>
);

export default TableComponent; 