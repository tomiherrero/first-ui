import {call, put, select} from 'redux-saga/effects';
import {fetchCountrySucceeded, submitCountrySucceeded, fetchCountriesSucceeded, deleteCountrySucceeded} from '../actions/country';
import CountryService from '../services/country';


export function* fetchCountry({filter}){
    const countries = yield call(CountryService.apiCall, filter)
    yield put(fetchCountrySucceeded(countries))
}

export function* fetchCountryOne({id}){
  const country = yield call(CountryService.apiCallOne, id)
  yield put(fetchCountriesSucceeded(country))
}

export function* submitCountry () {
  const {currentCountry} = yield select(state => state.country); 
  const {status, data} = yield call(CountryService.apiSave, currentCountry);
  yield put(submitCountrySucceeded(status, data));
}


export function* deleteCountry({id}){
  yield call(CountryService.apiDelete, id);
  yield put(deleteCountrySucceeded(true));
  yield call(fetchCountry, {});
}