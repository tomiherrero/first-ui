import {all, takeEvery} from 'redux-saga/effects'; 
import {FETCH_COUNTRY_REQUESTED,
            SUBMIT_COUNTRY_REQUESTED,
            FETCH_COUNTRIES_REQUESTED,
            DELETE_COUNTRY_REQUESTED
        } from '../actions/country'
import {fetchCountry, submitCountry, fetchCountryOne,deleteCountry } from './country'
import { FETCH_PROVINCE_REQUESTED, SUBMIT_PROVINCE_REQUESTED, FETCH_PROVINCES_REQUESTED, DELETE_PROVINCE_REQUESTED } from '../actions/province';
import { fetchProvince, submitProvince, fetchProvinces, deleteProvince } from './province';



export default function* root () {
    yield all ([
        takeEvery(FETCH_COUNTRY_REQUESTED, fetchCountry),
        takeEvery(SUBMIT_COUNTRY_REQUESTED, submitCountry),
        takeEvery(FETCH_COUNTRIES_REQUESTED, fetchCountryOne),
        takeEvery(DELETE_COUNTRY_REQUESTED, deleteCountry),

        takeEvery(FETCH_PROVINCE_REQUESTED, fetchProvince),
        takeEvery(SUBMIT_PROVINCE_REQUESTED, submitProvince),
        takeEvery(FETCH_PROVINCES_REQUESTED, fetchProvinces),
        takeEvery(DELETE_PROVINCE_REQUESTED, deleteProvince)
    ]);
}