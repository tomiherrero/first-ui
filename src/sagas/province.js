import {call, put, select} from 'redux-saga/effects';
import ProvinceService from '../services/province';
import {fetchProvinceSucceeded, submitProvinceSucceeded, fetchProvincesSucceeded, deleteProvinceSucceeded} from '../actions/province'

export function* fetchProvince({filter}) {
    const provinces = yield call(ProvinceService.apiCall, filter)
    yield put(fetchProvinceSucceeded(provinces));
}

export function* submitProvince() {
    const {currentProvince} = yield select (state => state.province);
    const {status, data} = yield call(ProvinceService.apiSave, currentProvince);
    yield put(submitProvinceSucceeded(status, data));
    if (currentProvince.id) { 
        yield call(fetchProvince({id : currentProvince.id}))
    }
  }

export function* fetchProvinces({id}) {
    const province = yield  call(ProvinceService.apiCallOne, id);
    yield put(fetchProvincesSucceeded(province));
  }

export function* deleteProvince({id}) {
    yield call(ProvinceService.apiDelete, id);
    yield put(deleteProvinceSucceeded(true));
    yield call(fetchProvince, {});
  }
