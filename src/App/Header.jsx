import React, { useState }from 'react';
import {Link} from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

const Header = () => {
        const [collapsed, setCollapsed] = useState(true);
         const toggleNavbar = () => setCollapsed(!collapsed);
        return (
                <header>
                        <Navbar size= "sm"vertical color="light" light>
                                <NavbarBrand tag= {Link} to = "/" className="mr-auto">Home</NavbarBrand>
                                <NavbarToggler onClick={toggleNavbar} className="mr-2" />
                                <Collapse isOpen={!collapsed} navbar>
                                        <Nav navbar>
                                                <NavItem>
                                                        <NavLink tag={Link} to="/country">Paises</NavLink>
                                                </NavItem>
                                                <NavItem>
                                                        <NavLink tag={Link} to="/province">Provincias</NavLink>
                                                </NavItem>
                                        </Nav>
                                </Collapse>
                        </Navbar>
                </header>
        )
};
export default Header;