import React, {useState} from 'react'; 
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,

  } from 'reactstrap';

const Footer = () => {

    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    return( 
        <footer>
            <Navbar size="sm" vertical color="light" light>
                <Navbar color="light" light expand="sm ">
                    <NavbarBrand> Hecho por Tomás Herrero</NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                        <Collapse isOpen={isOpen} navbar />
                </Navbar>
             </Navbar> 
        </footer>
    )
 }  


export default Footer; 