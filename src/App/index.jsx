import React, {Fragment} from 'react'; 
import Header from './Header';
import Footer from './Footer';
import {
    HashRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import {Container, Row, Col}from 'reactstrap';
import Home from "./home";
import Country from "../pages/Country";
import Province from "../pages/Province";

const Index =() => (
        <Fragment>
                <Router>
                        <Container>
                                <Row>
                                        <Col>
                                                <Header/>
                                                <main>
                                                        < Switch>
                                                                <Route path= "/country" component = {Country}/>    
                                                                <Route path = "/province" component = {Province}/> 
                                                                <Route path = "/" component = {Home} />
                                                        </Switch>           
                                                </main>
                                                <Footer />
                                        </Col>
                                </Row>
                        </Container>
                </Router>
        </Fragment>
);
export default Index;