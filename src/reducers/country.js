import {
        FETCH_COUNTRY_REQUESTED, 
        FETCH_COUNTRY_SUCCEEDED,
        UPDATE_COUNTRY,
        FETCH_COUNTRIES_REQUESTED,
        FETCH_COUNTRIES_SUCCEEDED
} from '../actions/country'


const initialState = {
    country: [],
    currentCountry: {
      name: ' ',
      code: ' ',
      id: null
    },
    headers: [
        { 
          label: 'Nombre',
          key: 'name'
        },
        {
          label: 'Codigo',
          key: 'code'
        }, 
        {
          label: 'ID', 
          key: 'id'
        }
      ]
}

export default (state = initialState, action) =>{
    switch(action.type){
      case FETCH_COUNTRY_REQUESTED: 
        return {...state, country: []}
      case FETCH_COUNTRY_SUCCEEDED:
        return {...state, countries: action.countries}
      case FETCH_COUNTRIES_REQUESTED:
        return {...state, currentCountry: initialState.currentCountry}
      case FETCH_COUNTRIES_SUCCEEDED: 
        return {...state, currentCountry: action.country}
      case UPDATE_COUNTRY: 
        return {...state, currentCountry: action.countries}
      default: 
        return{...state};
    }
}