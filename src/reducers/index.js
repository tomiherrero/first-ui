import {combineReducers} from 'redux';
import country from './country';
import province from './province';
export default combineReducers({
    country,
    province
});