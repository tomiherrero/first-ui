import {FETCH_PROVINCE_REQUESTED,
            FETCH_PROVINCE_SUCCEEDED,
            UPDATE_PROVINCE,
            FETCH_PROVINCES_REQUESTED,
            FETCH_PROVINCES_SUCCEEDED
} from '../actions/province';

const initialState = {
    provinces: [],
    currentProvince: {
        name: ' ',
        code: ' ',
        id: null
    },
    headers: [
        { 
            label: 'Nombre',
            key: 'name'
          },
          {
            label: 'Codigo',
            key: 'code'
          }, 
          {
            label: 'ID', 
            key: 'id'
          }
        ]
}

export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_PROVINCE_REQUESTED:
            return {...state, provinces: []}
        case FETCH_PROVINCE_SUCCEEDED:
            return {...state, provinces: action.provinces}
        case FETCH_PROVINCES_REQUESTED:
            return {...state, currentProvince: initialState.currentProvince}
        case FETCH_PROVINCES_SUCCEEDED:
            return {...state, currentProvince: action.province}
        case UPDATE_PROVINCE: 
            return {...state, currentProvince: action.provinces}
        default: 
            return {...state};
    }
}